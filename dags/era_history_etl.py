
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator
from airflow.kubernetes.secret import Secret
from airflow.utils.dates import days_ago

# Secrets to env vars in pods
dbhost = Secret(
    deploy_type = "env",
    deploy_target = "DBHOST",
    secret = "catalyst-dev-user-secret",
    key = "dbhost"
)

dbport = Secret(
    deploy_type = "env",
    deploy_target = "DBPORT",
    secret = "catalyst-dev-user-secret",
    key = "dbport"
)

dbuser = Secret(
    deploy_type = "env",
    deploy_target = "DBUSER",
    secret = "catalyst-dev-user-secret",
    key = "dbuser"
)

dbpass = Secret(
    deploy_type = "env",
    deploy_target = "DBPASS",
    secret = "catalyst-dev-user-secret",
    key = "dbpass"
)

dbname = Secret(
    deploy_type = "env",
    deploy_target = "DBNAME",
    secret = "catalyst-dev-user-secret",
    key = "dbname"
)

cos_endpoint = Secret(
    deploy_type = "env",
    deploy_target = "COS_ENDPOINT",
    secret = "cos-user-secret",
    key = "cos_endpoint"
)

cos_key = Secret(
    deploy_type = "env",
    deploy_target = "COS_KEY",
    secret = "cos-user-secret",
    key = "cos_key"
)

cos_secret = Secret(
    deploy_type = "env",
    deploy_target = "COS_SECRET",
    secret = "cos-user-secret",
    key = "cos_secret"
)

cos_bucket = Secret(
    deploy_type = "env",
    deploy_target = "COS_BUCKET",
    secret = "cos-user-secret",
    key = "cos_bucket"
)

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "retries": 0,
}

dag = DAG(
    description = "ETL to import ERA history data, clean it and save it to parquet file in COS",
    dag_id="era_history_etl",
    default_args=default_args,
    start_date=days_ago(1),
    schedule_interval=None,
    #schedule_interval="0 1 * * *",
    catchup=False,
    tags=["era", "history", "etl"],
)

with dag:
    start = DummyOperator(
        task_id = "start"
    )
    
    stop = DummyOperator(
        task_id = "stop"
    )

    extract_era_data = KubernetesPodOperator(
            namespace               = "default",
            name                    = "extract",
            task_id                 = "extract",
            image                   = "registry.gitlab.com/eli.mayost/catalyst_dev_ge",
            startup_timeout_seconds = 120,
            cmds                    = ["python"],
            arguments               = ["/great_expectations/scripts/extract_era_data.py"],
            secrets                 = [
                cos_endpoint,
                cos_key,
                cos_secret,
                cos_bucket
            ]
    )

    load_to_temp_table = KubernetesPodOperator(
            namespace               = "default",
            name                    = "load",
            task_id                 = "load",
            image                   = "registry.gitlab.com/eli.mayost/catalyst_dev_ge",
            startup_timeout_seconds = 120,
            cmds                    = ["python"],
            arguments               = ["/great_expectations/scripts/load_reshaped_to_pg.py"],
            secrets                 = [
                dbhost,
                dbport,
                dbuser,
                dbpass,
                dbname,
                cos_endpoint,
                cos_key,
                cos_secret,
                cos_bucket
            ]
    )

    ge_reshaped = KubernetesPodOperator(
            namespace               = "default",
            name                    = "ge_reshaped",
            task_id                 = "ge_reshaped",
            image                   = "registry.gitlab.com/eli.mayost/catalyst_dev_ge",
            startup_timeout_seconds = 120,
            cmds                    = ["python"],
            arguments               = ["/great_expectations/scripts/run_opex_era_history_reshaped_checkpoint.py"],
            secrets                 = [
                cos_endpoint,
                cos_key,
                cos_secret,
                cos_bucket
            ]
    )
 
    clean_data = KubernetesPodOperator(
            namespace               = "default",
            name                    = "clean_data",
            task_id                 = "clean_data",
            image                   = "registry.gitlab.com/eli.mayost/catalyst_dev_ge",
            startup_timeout_seconds = 120,
            cmds                    = ["python"],
            arguments               = ["/great_expectations/scripts/clean_era_data.py"],
            secrets                 = [
                cos_endpoint,
                cos_key,
                cos_secret,
                cos_bucket
            ]
    )
 
    load_to_table = KubernetesPodOperator(
            namespace               = "default",
            name                    = "load_parquet",
            task_id                 = "load_parquet",
            image                   = "registry.gitlab.com/eli.mayost/catalyst_dev_ge",
            startup_timeout_seconds = 120,
            cmds                    = ["python"],
            arguments               = ["/great_expectations/scripts/load_parquet_to_pg.py"],
            secrets                 = [
                dbhost,
                dbport,
                dbuser,
                dbpass,
                dbname,
                cos_endpoint,
                cos_key,
                cos_secret,
                cos_bucket
            ]
    )

    ge_cleaned = KubernetesPodOperator(
            namespace               = "default",
            name                    = "ge_cleaned",
            task_id                 = "ge_cleaned",
            image                   = "registry.gitlab.com/eli.mayost/catalyst_dev_ge",
            startup_timeout_seconds = 120,
            cmds                    = ["python"],
            arguments               = ["/great_expectations/scripts/run_opex_era_history_cleaned_checkpoint.py"],
            secrets                 = [
                cos_endpoint,
                cos_key,
                cos_secret,
                cos_bucket
            ]
    )
 
start >> extract_era_data >> load_to_temp_table >> ge_reshaped >> clean_data >> load_to_table >> ge_cleaned >> stop 
