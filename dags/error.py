
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator
from airflow.utils.dates import days_ago

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "retries": 0,
}

dag = DAG(
    description = "Error DAG",
    dag_id="error_etl",
    default_args=default_args,
    start_date=days_ago(1),
    schedule_interval=None,
    #schedule_interval="0 1 * * *",
    catchup=False,
    tags=["error", "demo", "etl"],
)

with dag:
    start = DummyOperator(
        task_id = "start"
    )
    
    stop = DummyOperator(
        task_id = "stop"
    )

    sleep = KubernetesPodOperator(
            namespace               = "default",
            name                    = "sleep",
            task_id                 = "sleep",
            image                   = "registry.gitlab.com/eli.mayost/error",
            startup_timeout_seconds = 120,
            cmds                    = ["sleep"],
            arguments               = ["5"]
    )

    error = KubernetesPodOperator(
            namespace               = "default",
            name                    = "hello",
            task_id                 = "hello",
            image                   = "registry.gitlab.com/eli.mayost/error",
            startup_timeout_seconds = 120,
            cmds                    = ["python"],
            arguments               = ["/app/scripts/error.py"]
    )
 
start >> sleep >> error >> stop
