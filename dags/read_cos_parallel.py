
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator
from airflow.utils.dates import days_ago
from airflow.kubernetes.secret import Secret

cos_endpoint = Secret(
    deploy_type = "env",
    deploy_target = "COS_ENDPOINT",
    secret = "cos-user-secret",
    key = "cos_endpoint"
)

cos_key = Secret(
    deploy_type = "env",
    deploy_target = "COS_KEY",
    secret = "cos-user-secret",
    key = "cos_key"
)

cos_secret = Secret(
    deploy_type = "env",
    deploy_target = "COS_SECRET",
    secret = "cos-user-secret",
    key = "cos_secret"
)

cos_bucket = Secret(
    deploy_type = "env",
    deploy_target = "COS_BUCKET",
    secret = "cos-user-secret",
    key = "cos_bucket"
)
default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "retries": 0,
}

dag = DAG(
    description = "Read in parallel from COS",
    dag_id="read_cos_parallel_etl",
    default_args=default_args,
    start_date=days_ago(1),
    schedule_interval=None,
    #schedule_interval="0 1 * * *",
    catchup=False,
    tags=["cos", "parallel", "etl"],
)

with dag:
    start = DummyOperator(
        task_id = "start"
    )
    
    stop = DummyOperator(
        task_id = "stop"
    )

    read_1 = KubernetesPodOperator(
            namespace               = "default",
            name                    = "read_1",
            task_id                 = "read_1",
            image                   = "registry.gitlab.com/eli.mayost/catalyst_dev_ge",
            startup_timeout_seconds = 120,
            cmds                    = ["python"],
            arguments               = ["/great_expectations/scripts/read_1.py"],
            secrets                 = [
                cos_endpoint,
                cos_key,
                cos_secret,
                cos_bucket
            ]
    )

    read_2 = KubernetesPodOperator(
            namespace               = "default",
            name                    = "read_2",
            task_id                 = "read_2",
            image                   = "registry.gitlab.com/eli.mayost/catalyst_dev_ge",
            startup_timeout_seconds = 120,
            cmds                    = ["python"],
            arguments               = ["/great_expectations/scripts/read_2.py"],
            secrets                 = [
                cos_endpoint,
                cos_key,
                cos_secret,
                cos_bucket
            ]
    )
 
start >> [read_1, read_2] >> stop
