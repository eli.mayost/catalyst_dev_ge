
FROM python

MAINTAINER Eli Mayost <eli.mayost@cz.ibm.com>

# Update packages
RUN apt-get -y update

# Install git
RUN apt-get install -y git

# Install needed packages
RUN pip install sqlalchemy psycopg2 black great_expectations boto3 pandas pyarrow

# Make root directory
RUN mkdir -p /great_expectations

WORKDIR .

COPY great_expectations/data /great_expectations/data
COPY great_expectations/great_expectations.yml /great_expectations/great_expectations.yml
COPY great_expectations/expectations /great_expectations/expectations
COPY great_expectations/checkpoints /great_expectations/checkpoints
COPY great_expectations/scripts /great_expectations/scripts


