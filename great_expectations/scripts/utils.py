
import os
import boto3
import logging
from datetime import datetime as dt
from sqlalchemy import create_engine

# Logging
def setup_logging():
    logging.basicConfig(
        format="%(asctime)s | %(levelname)s | %(message)s",
        datefmt="%d/%m/%Y %H:%M:%S",
        #Change to DEBUG to see all, but very noisy
        level=logging.INFO 
    )

#def write_log(level, message):
#    now = dt.strftime(dt.now(), "%d/%m/%Y %H:%M:%S")
#    print(f"{now} | {level} | {message}", flush = True)


def get_s3_client():
    # COS config
    cos_endpoint = os.environ['COS_ENDPOINT']
    cos_key      = os.environ['COS_KEY']
    cos_secret   = os.environ['COS_SECRET']
    cos_bucket   = os.environ['COS_BUCKET']
    
    # COS session/client
    session = boto3.session.Session()
    s3_client = session.client(
        service_name          = 's3',
        endpoint_url          = cos_endpoint,
        aws_access_key_id     = cos_key,
        aws_secret_access_key = cos_secret
    )

    return (s3_client, cos_bucket)

def get_pg_connection():
    db_host = os.environ['DBHOST']
    db_port = os.environ['DBPORT']
    db_user = os.environ['DBUSER']
    db_pass = os.environ['DBPASS']
    db_name = os.environ['DBNAME']

    engine  = create_engine(f"postgresql+psycopg2://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}")
    conn = engine.connect()

    return conn
