
import logging
from utils import setup_logging, get_s3_client

# Setup logging
setup_logging()

# Get S3 client for COS
(s3_client, cos_bucket) = get_s3_client()

# Download raw data and save in container's file system
logging.info("Getting raw data from COS")
s3_client.download_file(Bucket = cos_bucket, Key = 'era_historical.csv', Filename = "/great_expectations/data/era_history.csv")
logging.info("Successfully downloaded the raw data from COS")
