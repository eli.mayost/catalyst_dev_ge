
import logging
import pandas as pd
from sqlalchemy import Table, Boolean, Column, String, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base
from utils import setup_logging, get_s3_client, get_pg_connection
from uuid import uuid4

# Setup logging
setup_logging()

# Get S3 client for COS
(s3_client, cos_bucket) = get_s3_client()

# Get parquet file from COS
logging.info("Getting cleaned data from COS.")
s3_client.download_file(Filename = "/great_expectations/data/era_history_clean.parquet", Bucket = cos_bucket, Key = "era_history_clean.parquet")
logging.info("Successfully got cleaned data from COS.")

# Define Table
CatalystBase = declarative_base()
metadata = CatalystBase.metadata

era_history = Table(
    'era_history',
    metadata,
    Column('id'           , UUID(as_uuid = True) , primary_key = True) ,
    Column('account_name' , String               , nullable = False)   ,
    Column('gbgid'        , String               , nullable = False)   ,
    Column('contract_id'  , String               , nullable = False)   ,
    Column('completed'    , String               , nullable = False)   ,
    Column('day'          , Integer              , nullable = False)   ,
    Column('month'        , Integer              , nullable = False)   ,
    Column('quarter'      , Integer              , nullable = False)   ,
    Column('year'         , Integer              , nullable = False)   ,
    Column('score'        , Integer              , nullable = False)   ,
    Column('q1'           , Boolean              , nullable = False)   ,
    Column('q2'           , Boolean              , nullable = False)   ,
    Column('q3'           , Boolean              , nullable = False)   ,
    Column('q4'           , Boolean              , nullable = False)   ,
    Column('q5'           , Boolean              , nullable = False)   ,
    Column('q6'           , Boolean              , nullable = False)   ,
    Column('q7'           , Boolean              , nullable = False)   ,
    Column('q8'           , Boolean              , nullable = False)   ,
    Column('q9'           , Boolean              , nullable = False)   ,
    Column('q10'          , Boolean              , nullable = False)   ,
    Column('q11'          , Boolean              , nullable = False)   ,
    Column('q12'          , Boolean              , nullable = False)   ,
    Column('q13'          , Boolean              , nullable = False)   ,
    Column('q14'          , Boolean              , nullable = False)   ,
    Column('q15'          , Boolean              , nullable = False)   ,
    Column('q16'          , Boolean              , nullable = False)   ,
    Column('comments'     , String               , nullable = True)   ,
)

# Get connection to DB
conn = get_pg_connection()
metadata.create_all(conn)

# Create table
logging.info("Creating PG table.")
metadata.create_all(conn)
logging.info("Successfully created PG table.")

# Read parquet file to pandas dataframe
df = pd.read_parquet("/great_expectations/data/era_history_clean.parquet")

# Generate UUIDs to add to dataframe
ids = pd.DataFrame([uuid4() for _ in range(len(df.index))], columns = ['id'])

df = pd.concat([ids, df], axis = 'columns')

# Write dataframe to PG
logging.info("Write parquet file to PG table.")
df.to_sql('era_history', con = conn, index = False, if_exists = 'append')
logging.info("Successfully written parquet file to PG table.")

