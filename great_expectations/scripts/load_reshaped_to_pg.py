
import logging
import pandas as pd
from utils import setup_logging, get_s3_client, get_pg_connection

# Setup logging
setup_logging()

# Get S3 client for COS
(s3_client, cos_bucket) = get_s3_client()

# Download reshaped data and save in container's file system
logging.info("Getting reshaped data from COS")
s3_client.download_file( Bucket = cos_bucket, Key = 'era_history_reshaped.csv', Filename = "/great_expectations/data/era_history_reshaped.csv")
logging.info("Successfully downloaded the reshaped data from COS")

# Read file with pandas
df = pd.read_csv("/great_expectations/data/era_history_reshaped.csv", sep = "|")

# Get connection to PG and write the dataframe to a table
conn = get_pg_connection()

logging.info("Writing reshaped data to temporary database table.")
df.to_sql(name = "era_history_reshaped", con = conn, index = False, if_exists = "replace")
logging.info("Successfully written reshaped data to temporary database table.")

