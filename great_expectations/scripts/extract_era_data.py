
import logging
import dateutil.parser as dparser
from re import search
from math import ceil
from csv import reader
from utils import setup_logging, get_s3_client

# Setup logging
setup_logging()

# Get S3 client for COS
(s3_client, cos_bucket) = get_s3_client()

# Download raw data and save in container's file system
logging.info("Getting raw data from COS")
s3_client.download_file(Bucket = cos_bucket, Key = 'era_historical.csv', Filename = "/great_expectations/data/era_history.csv")
logging.info("Successfully downloaded the raw data from COS")

# Start extraction of raw data
logging.info("Extracting and reshaping the raw data.")

# set delimiter
delimiter = '|'

# files for reading and writing
in_file  = '/great_expectations/data/era_history.csv'
out_file = '/great_expectations/data/era_history_reshaped.csv'

# read file line by line
with open(in_file, 'r') as in_line, \
     open(out_file, 'a') as out_line:

    # print header to out file
    print(
        "account_name",
        "gbgid",
        "contract_id",
        "completed",
        "day",
        "month",
        "quarter",
        "year",
        "score",
        "q1",
        "q2",
        "q3",
        "q4",
        "q5",
        "q6",
        "q7",
        "q8",
        "q9",
        "q10",
        "q11",
        "q12",
        "q13",
        "q14",
        "q15",
        "q16",
        "comments",
        sep = delimiter,
        file = out_line    
    )

    # Read in in file
    csv_reader = reader(in_line, delimiter = delimiter)
    for contract_id, account_name, gbgid, *rest in csv_reader:
        # chunk answers into 19 chunks array
        answers = [rest[i:i+19] for i in range(0, len(rest), 19)]
        for a in answers:
            [
            completed,
            score,
            q1,
            q2,
            q3,
            q4,
            q5,
            q6,
            q7,
            q8,
            q9,
            q10,
            q11,
            q12,
            q13,
            q14,
            q15,
            q16,
            comments
            ] = a

            # if completed text is not None, we try and extract the date
            # the assessment was submited on
            if completed:
                m = search(r'\d{1,4}/\d{1,4}/\d{1,4}', completed)
            
                # if a date was found
                if m:
                    date = dparser.parse(m.group(), fuzzy = True)
                    year = date.year
                    day = date.day
                    month = date.month
                    quarter = ceil(date.month/3.)
                # if a date was not found
                else:
                    year = quarter = day = month = ""

                comments = "".join(comments.splitlines())

                # print the record details to out file
                print(
                    account_name,
                    gbgid,
                    contract_id,
                    completed,
                    day,
                    month,
                    quarter, 
                    year,
                    score,
                    q1,
                    q2,
                    q3,
                    q4,
                    q5,
                    q6,
                    q7,
                    q8,
                    q9,
                    q10,
                    q11,
                    q12,
                    q13,
                    q14,
                    q15,
                    q16,
                    f"\"{comments}\"",
                    sep = delimiter,
                    file = out_line    
                )

logging.info("Uploading reshaped raw data to COS.")
s3_client.upload_file(Filename = out_file, Bucket = cos_bucket, Key = "era_history_reshaped.csv")
logging.info("Successfully uploaded reshaped raw data to COS.")
