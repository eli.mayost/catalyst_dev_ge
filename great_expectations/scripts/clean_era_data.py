
import logging
import pandas as pd
from re import match
from utils import setup_logging, get_s3_client

# Setup logging
setup_logging()

# Get S3 client fro COS
(s3_client, cos_bucket) = get_s3_client()

# Get the reshaped data
# Download raw data and save in container's file system
logging.info("Getting reshaped data from COS")
s3_client.download_file(Bucket = cos_bucket, Key = 'era_history_reshaped.csv', Filename = "/great_expectations/data/era_history_reshaped.csv")
logging.info("Successfully downloaded the reshaped data from COS")

# Clean the data
df = pd.read_csv("/great_expectations/data/era_history_reshaped.csv", sep = "|")

# Change the question from Yes/No to boolean type
cols = [c for c in df.columns if match('^q[0-9]{1,2}', c)]

for col in cols:
    df[col] = df[col].map({'Yes': True, 'No': False})

# Change day/month/year to integer and deal with NA values
for col in ['day', 'month', 'quarter', 'year']:
    df[col] = df[col].fillna(0)
    df[col] = df[col].astype(int)

# Change score from string to int
df['score'] = df['score'].fillna(-1)
df['score'] = df['score'].astype(int)

# Remove nulls from comments
df['comments'] = df['comments'].fillna("")

# Save to parquet
df.to_parquet("/great_expectations/data/era_history_clean.parquet")

# Save clean data back to COS
logging.info("Uploading cleaned data to COS.")
s3_client.upload_file(Filename = "/great_expectations/data/era_history_clean.parquet", Bucket = cos_bucket, Key = "era_history_clean.parquet")
logging.info("Successfully uploaded cleaned data to COS.")

