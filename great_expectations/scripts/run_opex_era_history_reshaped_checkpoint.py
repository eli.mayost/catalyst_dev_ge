
import os
import logging
import subprocess
import great_expectations as ge
from sys import exit
from utils import setup_logging, get_s3_client

# Setup logging
setup_logging()

# Get S3 clien for COS
(s3_client, cos_bucket) = get_s3_client()

# get GE context
ctx = ge.get_context()

# run the checkpoint
logging.info("Start running opex_era_history_reshaped checkpoint")
res = ctx.run_checkpoint('opex_era_history_reshaped')

# once the checkpoint ran, push the updated data docs to COS
#cmd = '/great_expectations/scripts/data_docs_to_cos.py reshaped'
#subprocess.call(cmd, shell = True)
# Bundle files together
cmd = f"tar -czf /great_expectations/uncommitted/data_docs/local_site/opex_era_history_reshaped.tar.gz /great_expectations/uncommitted/data_docs/local_site/* 2>/dev/null"
subprocess.call(cmd, shell = True)

# COS upload key
[date, time, *rest] = os.listdir(f"/great_expectations/uncommitted/data_docs/local_site/validations/era_history_reshaped/warning/")[0].split('-')

upload_key = f"ge/reshaped/{date}/{time}/opex_era_history_reshaped.tar.gz"

# Upload to COS
logging.info(f"Start uploading reshaped report to COS")
s3_client.upload_file(Bucket = cos_bucket, Key = upload_key, Filename = f"/great_expectations/uncommitted/data_docs/local_site/opex_era_history_reshaped.tar.gz")
logging.info(f"Uploaded reshaped report to COS successfuly")

# Bubble up the failure to airflow
if not res.success:
    exit("Validations failed. Exiting.")


