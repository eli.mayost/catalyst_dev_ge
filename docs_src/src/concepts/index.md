
### Airflow concepts

#### DAGs

Directed Acyclic Graph - is a collection of all the tasks you want to run, organized in a way that reflects their relationships and dependencies.

#### Task

Defines a unit of work within a DAG; it is represented as a node in the DAG graph, and it is written in Python.

#### Task Lifecycle

 - No status (scheduler created empty task instance)
 - Scheduled (scheduler determined task instance needs to run)
 - Queued (scheduler sent task to executor to run on the queue)
 - Running (worker picked up a task and is now running it)
 - Success (task completed)

### GE concepts

#### Data context

The data context includes all the information about your project.
That includes configuration, data sources, test suites, checkpoints and jupyter notebooks.

#### Data source

A data source is all the configuration that is needed to connect to your data.
This configuration includes (if a DB), host, port, username, password, database name, drive to be used, runtime to be used etc...

#### Test suite

The test suite is where your expectations exist.

#### Checkpoint

The checkpoint is responsible to take a batch of data and validate it against you expectations.

### How to generate all the above

All the above concepts are nothing more than python classes and could be used directly, but GE provides CLI commands and notebooks to ease the creation of all the above.

We will use the V3 API so remember to use it like so:

::: tip CLI
great_expectations --v3-api
:::

To avoid forgetting, you can add an alias in your ~/.bashrc:

``` bash
alias ge='great_expectations --v3-api'
```
