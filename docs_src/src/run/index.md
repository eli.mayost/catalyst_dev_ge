
### Activate DAG

You should have only one DAG.

Activate it.

<br /><br />
<a class="zoom" href="../activate_dag.png" target="_blank">
  <img :src="$withBase('/activate_dag.png')" alt="infra">
</a>
<br /><br />

### Run DAG

Trigger the DAG.

It has two steps; Running the testing suite through the checkpoint, and upload the resulting documents to COS.

<br /><br />
<a class="zoom" href="../trigger_dag.png" target="_blank">
  <img :src="$withBase('/trigger_dag.png')" alt="infra">
</a>
<br /><br />


<style>
.zoom {
  cursor: zoom-in -webkit-zoom-in;
}
</style>

