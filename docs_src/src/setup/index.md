
### 1) Install needed software

  - [Docker](https://www.docker.com/) or [Podman](https://podman.io/) for RHEL
  - Local Kubernetes cluster (using [Minikube](https://minikube.sigs.k8s.io/docs/))
  - Easy management of the cluster (using [k9s](https://k9scli.io/)) 
  - [Rclone](https://rclone.org/)
  - The Gihub repo [here](https://github.kyndryl.net/eli-mayost/ge_demo.git)

### 2) Files to create locally (mainly .env and secrets not to be pushed to the repo)

#### k8/catalyst-dev-user-secret.yaml

Fill in base64 encoded string for dbuser and dbpass.

``` yaml
apiVersion: v1
kind: Secret
metadata:
  name: catalyst-dev-user-secret
data:
  dbhost: xxxxxxxxxx 
  dbport: xxxxxxxxxx 
  dbuser: xxxxxxxxxx 
  dbpass: xxxxxxxxxx 
  dbname: xxxxxxxxxx 
```

#### k8s/git-sync-ssh-secret.yaml

Generate a ssh key:

``` bash
ssh-keygen -t rsa
```

Base64 encode the public key:

``` bash
base64 ~/.ssh/[KEYNAME].pub
```

Copy the string to the secret:

``` yaml
apiVersion: v1
kind: Secret
metadata:
  name: git-sync-ssh-secret
data:
  gitSshKey: xxxxxxxxxx 
```

#### k8s/cos-user-secret.yaml

Fill in the details of the COS bucket to be used for uploading the reports.

``` yaml
apiVersion: v1
kind: Secret
metadata:
  name: cos-user-secret
data:
  cos_endpoint: xxxxxxxxxx 
  cos_key: xxxxxxxxxx 
  cos_secret: xxxxxxxxxx 
  cos_bucket: xxxxxxxxxx 
```

### 3) Create local cluster

::: tip
Change the parameters in the Makefile to suit your machine.
:::

``` bash
make start-cluster 
```

Apply secrets.

``` bash
make apply-secrets
```

Deploy airflow.

``` bash
make deploy-airflow
```

::: danger
Once you login with the default admin user, create yourself an account, and delete the admin account.
:::

### 4) Create catalyst database

Forward the port of the postgresql database to be able to access it locally:

``` bash
kubectl port-forward svc/airflow-postgresql 5432:5432
```

Connect and create the catalyst database:

``` bash
postgresql -h localhost -U postgres
```

``` sql
CREATE DATABASE catalyst;
```

