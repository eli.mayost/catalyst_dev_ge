
### Setup a remote object storage service

``` bash
rclone config
```

You will be presented with an interactive menu. Fill in the details.

### Download the results from COS

You can log in cloud.ibm.com and navigate to the storage and find the file in your bucket.

If you have installed and configured rclone, you can retrieve the file like so:

``` bash
rclone copy -P cos://[BUCKET NAME HERE]/catalyst_ge/[reshaped | cleaned]/[YYYYMMDD]/[HHMMSS]/opex_era_history_[reshaped | cleaned].tar.gz [LOCAL DIR HERE]
```

You can list all the objects in your bucket like so:

``` bash
rclone lsl cos://[BUCKET NAME HERE]
```

Once you have copied the tar.gz archive to a local directory, you can uncompress it like so:

``` bash
tar -xzvf [NAME OF ARCHIVE HERE]
```

You can then navigate in until you see the index.html file:

``` bash
cd great_expectations/uncommitted/data_docs/local_site/
```

Open index.html with a web browser:

``` bash
google-chrome index.html
````

<br /><br />

Click on the row with the results:

<br /><br />
<a class="zoom" href="../results_1.png" target="_blank">
  <img :src="$withBase('/results_1.png')" alt="infra">
</a>
<br /><br />

You can then filter only for the failed expectations if you wish to do so:

<br /><br />
<a class="zoom" href="../results_2.png" target="_blank">
  <img :src="$withBase('/results_2.png')" alt="infra">
</a>
<br /><br />

<style>
.zoom {
  cursor: zoom-in -webkit-zoom-in;
}
</style>

