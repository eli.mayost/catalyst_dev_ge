
### Overview

#### Apache Airflow

Apache Airflow is an open-source workflow management platform for data engineering pipelines.

#### Great Expectations

Great Expectations (GE) is the leading tool for validating, documenting, and profiling your data to maintain quality and improve communication between teams.

We will demo the validating and documenting aspects.

#### Deployment

The demo will be deployed on a Kubernetes cluster.

The data files and the GE validation results will be then stored in IBM Cloud Object Storage (COS).

<br /><br />
<a class="zoom" href="../overview.png" target="_blank">
  <img :src="$withBase('/overview.png')" alt="infra">
</a>
<br /><br />

<style>
.zoom {
  cursor: zoom-in -webkit-zoom-in;
}
</style>

