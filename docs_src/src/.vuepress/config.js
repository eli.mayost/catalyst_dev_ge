//const { description } = require('../../package')

module.exports = {
  base: '/eli-mayost/ge_demo/',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Airflow / Great Expectations Demo',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: ' ',

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    search: false,
    nav: [
    ],
    sidebar: [
      {
        title: 'Overview',
        path: '/overview/'
      },  
      {
        title: 'Concepts',
        path: '/concepts/'
      },  
      {
        title: 'Setup',
        path: '/setup/'
      },  
      {
        title: 'Pipeline steps',
        path: '/steps/'
      },  
      {
        title: 'Run pipeline',
        path: '/run/'
      },  
      {
        title: 'Results',
        path: '/results/'
      },  
    ]
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    //'@vuepress/plugin-back-to-top',
    //'@vuepress/plugin-medium-zoom',
    'copy-code'
  ]
}
