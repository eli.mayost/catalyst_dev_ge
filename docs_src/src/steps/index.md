
### Pipeline's steps

#### extract

Extract the data from the data source.
In our example, the data was given in a wide format, so we extract it and convert it to a long format.

#### load

Load the data from the previsou step into a temporary PG table.

#### ge_reshaped 

Test that the data has the expected shape and columns.

#### clean_data

Clean the data. Remove NAs and set the correct types for columns. Save to parquet file.

#### load_parquet

Load the parquet file in a PG table.

#### ge_cleaned

Test that the parquet file is conform to our expectations.

