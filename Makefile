
include great_expectations/.env

TAG=$(shell git rev-parse --short HEAD)

registry-login:
	@-podman login registry.gitlab.com -u "$(GITLAB_USERNAME)" -p "$(GITLAB_TOKEN)"

build-image:
	@-podman build -t registry.gitlab.com/eli.mayost/catalyst_dev_ge:$(TAG) .
	@-podman tag registry.gitlab.com/eli.mayost/catalyst_dev_ge:$(TAG) registry.gitlab.com/eli.mayost/catalyst_dev_ge:latest

push-image: registry-login
	@-podman push registry.gitlab.com/eli.mayost/catalyst_dev_ge:$(TAG)
	@-podman push registry.gitlab.com/eli.mayost/catalyst_dev_ge:latest

build-and-push: registry-login build-image push-image

start-cluster:
	minikube start --cpus 2 --memory 8g --driver podman

stop-cluster:
	minikube stop

apply-secrets:
	kubectl apply -f k8s/catalyst-dev-user-secret.yaml 
	kubectl apply -f k8s/git-sync-ssh-secret.yaml
	kubectl apply -f k8s/cos-user-secret.yaml

deploy-airflow:
	helm upgrade --install airflow apache-airflow/airflow -f k8s/values.yaml --debug

undeploy-airflow:
	helm uninstall airflow --debug
